# Redis@Docker

https://redis.io/docs/

# TLS

This docker solution depends on TLS including inter node communication in cluster.
Not encrypted traffic is not supported, so clients must also connect via TLS.

## PKI

PKI is a requirement for using mTLS. Initialize PKI with following commands:

```
cd ./pki
./gen-ca 'Redis CA'
./mk-csr redis.example.com '*.redis.example.com'
./sign redis.example.com.csr
```

## mTLS

Notice: Uses forced client certificate.
No Cert -> no access

# Set Password

```
ACL SETUSER myuser on >ReallyBadPassword
ACL SAVE
```

You must sync passwords so all users are known to cluster. Script *sync-acl* will do that,
please do not forget to modify it so all cluster nodes will reload latest users.acl.
In case of using remote cluster nodes - strongly recommeded - you have to copy users.acl
to all remote nodes. Use either ssh with private/public key authentication or a
central https server with forced mTLS.

# Test

Start containers and check following steps:

* ./redis-cli -> set aaa triple_a
* ./redis-cli -> set hello world
* ./redis-cli -> set hhh triple_h
* ./redis-cli -> set zzz triple_z
* ./redis-cli -> get aaa
* ./redis-cli -> get hello
* ./redis-cli -> get hhh
* ./redis-cli -> get zzz
* stop cluster with *docker compose down*
* restart cluster with *./restart -v* - look out for any errors or warnings
* ./redis-cli -> get aaa
* ./redis-cli -> get hello
* ./redis-cli -> get hhh
* ./redis-cli -> get zzz

# Debug

```
docker-compose exec redis1 bash
apt update && apt install iproute2 iputils-ping procps
./redis-cli --cluster info 127.0.0.1:6373
./redis-cli -c -h 172.20.0.31 -p 6373
```

# Network Considerations

A cluster makes sense on distributed hardware, only. Example for 3 servers:

host (1): 172.18.1.0/24, fd00:172:18:1::/64

host (2): 172.18.2.0/24, fd00:172:18:2::/64

host (3): 172.28.3.0/24, fd00:172:18:3::/64

FRR is installed on all host using OSPF or IS-IS for dynamic routing. This works also in large networks,
because most professional routers also supporting OSPF and IS-IS on IPv4 and IPv6.

Do not forget to set up iptables DOCKER-USER so red_cluster is reachable for cluster nodes.

These networks should be connected by a dedicated VLAN.

