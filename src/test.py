#!/usr/bin/env python3

from redis.cluster import RedisCluster
from redis.commands.json.path import Path
import redis.commands.search.aggregation as aggregations
import redis.commands.search.reducers as reducers
from redis.commands.search.field import TextField, NumericField, TagField
from redis.commands.search.indexDefinition import IndexDefinition, IndexType
from redis.commands.search.query import NumericFilter, Query
import base64
import os

user1 = {
    "name": "Paul John",
    "email": "paul.john@example.com",
    "age": 42,
    "city": "London"
}
user2 = {
    "name": "Eden Zamir",
    "email": "eden.zamir@example.com",
    "age": 29,
    "city": "Tel Aviv"
}
user3 = {
    "name": "Paul Zamir",
    "email": "paul.zamir@example.com",
    "age": 35,
    "city": "Tel Aviv"
}

rc = RedisCluster(
  host='redis-node-1', 
  port=6373,
  username="default", # use your Redis user. More info https://redis.io/docs/management/security/acl/
  password=os.environ.get('REDISCLI_AUTH'), # use your Redis password
  ssl=True,
  ssl_ca_certs="/usr/local/share/ca-certificates/redis-ca.crt",
  ssl_certfile="/usr/local/share/ca-certificates/user-admin.crt",
  ssl_keyfile="/usr/local/share/ca-certificates/user-admin.key"
)

print(rc.get_nodes())

rc.set('foo2', 'bar2')
print(rc.get('foo2'))

print(rc.get('foo'))
print(rc.get('hello'))
print(rc.get('aaa'))
print(rc.get('hhh'))
print(rc.get('zzz'))

rc.hset('user-session:123', mapping={
    'name': 'John',
    "surname": 'Smith',
    "company": 'Redis',
    "age": 29
})

print(rc.hgetall('user-session:123'))

# Save and retrieve a PNG file
with open("apu1_ex2200.jpeg", mode="rb") as bin_file:
  contents = bin_file.read()
  rc.set('jpeg1', contents)

binary_fc=rc.get('jpeg1')
base64_utf8_str = base64.b64encode(binary_fc).decode('utf-8')
dataurl = f'data:image/jpeg;base64,{base64_utf8_str}'
print(dataurl)

