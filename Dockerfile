from debian:bullseye-slim

run DEBIAN_FRONTEND=noninteractive apt-get update
run DEBIAN_FRONTEND=noninteractive apt-get install -q -y apt
run DEBIAN_FRONTEND=noninteractive apt update && apt -q -y full-upgrade

run DEBIAN_FRONTEND=noninteractive apt update && apt -q -y install \
  python3-pip \
  inetutils-ping \
  openssl

RUN \
  pip3 install redis

ARG REDISCLI_AUTH

CMD [ "sh",  "-c",  "tail -f /dev/null" ]

